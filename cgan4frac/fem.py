from dolfin import assemble_system, Constant, DirichletBC, dof_to_vertex_map, Function, FunctionSpace, MeshFunction, near, solve, SubDomain, TestFunction, TrialFunction, UnitSquareMesh, vertices
import numpy as np
import os
from ufl import FiniteElement, grad, inner, dx


class FEMModel(object):
	def __init__(self, argsdict):
		super(FEMModel, self).__init__()
		for key in argsdict.keys():
			setattr(self, key, argsdict[key])
		if not os.path.isdir(self.workdir):
			os.makedirs(self.workdir, exist_ok=True)
		self.coarse_mesh = UnitSquareMesh(self.target_shape - 1, self.target_shape - 1, 'left/right')
		self.fine_mesh = UnitSquareMesh(self.target_shape - 1, self.target_shape - 1, 'crossed')
		P3e = FiniteElement('CG', self.fine_mesh.ufl_cell(), 3)
		self.P3 = FunctionSpace(self.fine_mesh, P3e)
		self.boundaries = MeshFunction("size_t", self.fine_mesh, self.fine_mesh.topology().dim() - 1)
		self.boundaries.set_all(0)
		class Left(SubDomain):
			def inside(self, x, on_boundary):
				return (near(x[0], 0.) and on_boundary)
		class Right(SubDomain):
			def inside(self, x, on_boundary):
				return (near(x[0], 1.) and on_boundary)
		class Bottom(SubDomain):
			def inside(self, x, on_boundary):
				return (near(x[1], 0.) and on_boundary)
		class Top(SubDomain):
			def inside(self, x, on_boundary):
				return (near(x[1], 1.) and on_boundary)
		left = Left()
		left.mark(self.boundaries, 1)
		right = Right()
		right.mark(self.boundaries, 2)
		bottom = Bottom()
		bottom.mark(self.boundaries, 3)
		top = Top()
		top.mark(self.boundaries, 4)

	def __call__(self, a):
		a = self.get_function_from_data(a)
		p1, p2 = self.solve_fluid_flow_single(a)
		return self.get_data_from_function(p1, p2)

	def get_function_from_data(self, data):
		V_coarse = FunctionSpace(self.coarse_mesh, FiniteElement('CG', self.coarse_mesh.ufl_cell(), 1))
		u_coarse = Function(V_coarse)
		data_vector = np.zeros(self.coarse_mesh.num_vertices())
		for vertex in vertices(self.coarse_mesh):
			i = round(vertex.x(1)*(self.target_shape - 1))
			j = round(vertex.x(0)*(self.target_shape - 1))
			data_vector[vertex.index()] = data[i][j]
		u_coarse.vector().set_local(data_vector[dof_to_vertex_map(V_coarse)])
		u_coarse.vector().apply('insert')
		V_fine = FunctionSpace(self.fine_mesh, FiniteElement('CG', self.fine_mesh.ufl_cell(), 1))
		u_fine = Function(V_fine)
		u_fine.interpolate(u_coarse)
		return u_fine

	def get_data_from_function(self, *funcs):
		datas = tuple([])
		for func in funcs:
			data = np.zeros((self.target_shape, self.target_shape))
			for vertex in vertices(self.coarse_mesh):
				i = round(vertex.x(1)*(self.target_shape - 1))
				j = round(vertex.x(0)*(self.target_shape - 1))
				data[i][j] = func(vertex.point())
			datas += tuple([data])
		return datas

	def solve_fluid_flow_single(self, a):
		p = TrialFunction(self.P3)
		q = TestFunction(self.P3)
		L = inner((a**3/(12*Constant(1.e-3)))*grad(p), grad(q))*dx
		f = Constant(0.)*q*dx
		bcs = lambda pvalues, marks: [DirichletBC(self.P3, Constant(pvalues[i]), self.boundaries, marks[i]) for i in range(len(marks))]
		A, b = assemble_system(L, f, bcs([1.e3, 0.], [1, 2]))
		p1 = Function(self.P3)
		solve(A, p1.vector(), b, 'umfpack')
		A, b = assemble_system(L, f, bcs([1.e3, 0.], [3, 4]))
		p2 = Function(self.P3)
		solve(A, p2.vector(), b, 'umfpack')
		return p1, p2