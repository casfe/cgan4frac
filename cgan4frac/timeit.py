from contextlib import contextmanager
from time import perf_counter


class TimeIt(object):
	def __init__(self):
		super(TimeIt, self).__init__()
		self.time_stack = []

	def push_time(self):
		self.time_stack.append(perf_counter())

	def pop_time(self):
		return perf_counter() - self.time_stack.pop()

	@contextmanager
	def timethis(self):
		self.push_time()
		try:
			yield
		finally:
			self.run_time = self.pop_time()

	def get_run_time(self):
		return self.run_time