import numpy as np
import os
import pickle


def write_to_file(filepath, data, wtype='wb', use_pickle=True):
	filedir = os.path.split(filepath)[0]
	if not os.path.isdir(filedir):
		os.makedirs(filedir, exist_ok=True)
	file = open(filepath, wtype)
	pickle.dump(data, file) if use_pickle else np.savetxt(file, data)
	file.close()


def read_from_file(filepath, rtype='rb'):
	file = open(filepath, rtype)
	data = np.array(pickle.load(file))
	file.close()
	return data