from .io import read_from_file
import matplotlib
matplotlib.use('Agg')
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import os
from .cgan import get_exact_layered_tensor, get_principal_components, get_relative_error


clrs = np.array(['#fd411e', '#f075e6', '#0d75f8', '#02c14d', '#fffb00', '#fec615'])


def flip_data(*args):
	return tuple([np.flipud(arg) for arg in args])


def human_format(num):
	magnitude = 0
	while abs(num) >= 1000:
		magnitude += 1
		num /= 1000.0
	if magnitude <= 1:
		return '%.0f%s'%(num, 'k')
	elif magnitude == 2:
		return '%.2f%s'%(num, 'M')


class DataPlotter(object):
	def __init__(self, argsdict):
		super(DataPlotter, self).__init__()
		for key in argsdict.keys():
			setattr(self, key, argsdict[key])
		self.M_states = False
		if os.path.isdir(os.path.join(self.workdir, 'state')):
			self.state_files = [f for f in os.listdir(os.path.join(self.workdir, 'state')) if os.path.isfile(os.path.join(self.workdir, 'state', f))]
			self.state_files.sort(key=lambda x: float(x.split('_')[1].split('.')[0]))
			self.M_states = len(self.state_files)
			self.selected_state = range(self.M_states)[self.selected_state]
		self.skip = (slice(None, None, int(self.target_shape/16)), slice(None, None, int(self.target_shape/16)))
		self.xx, self.yy = np.meshgrid(np.linspace(0., 1., self.target_shape), np.linspace(0., 1., self.target_shape))

	def four_subplots(self, sub1, title1, sub2, title2, sub3, title3, sub4, title4, fname):
		sub1, sub2, sub3, sub4 = flip_data(sub1, sub2, sub3, sub4)
		fig, (ax1, ax2, ax3, ax4) = plt.subplots(figsize=(13, 5), ncols=4)
		pos1 = ax1.imshow(sub1, cmap='viridis', interpolation='nearest', extent=[0., 1., 0., 1.])
		ax1.set_title(title1)
		ax1.set_aspect('equal')
		fig.colorbar(pos1, ax=ax1, orientation="horizontal", pad=0.2)
		pos2 = ax2.imshow(sub2, cmap='viridis', interpolation='nearest', extent=[0., 1., 0., 1.])
		ax2.set_title(title2)
		ax2.set_aspect('equal')
		fig.colorbar(pos2, ax=ax2, orientation="horizontal", pad=0.2)
		pos3 = ax3.imshow(sub3, vmin=sub2.min(), vmax=sub2.max(), cmap='viridis', interpolation='nearest', extent=[0., 1., 0., 1.])
		ax3.set_title(title3)
		ax3.set_aspect('equal')
		fig.colorbar(pos3, ax=ax3, orientation="horizontal", pad=0.2)
		pos4 = ax4.imshow(sub4, cmap='viridis', interpolation='nearest', extent=[0., 1., 0., 1.])
		ax4.set_title(title4)
		ax4.set_aspect('equal')
		fig.colorbar(pos4, ax=ax4, orientation="horizontal", pad=0.2)
		filedir = os.path.split(fname)[0]
		if not os.path.isdir(filedir):
			os.makedirs(filedir, exist_ok=True)
		plt.savefig(fname='{}.png'.format(fname))
		plt.savefig(fname='{}.pdf'.format(fname))
		plt.close()

	def ellipse_plot(self, a, Ks, labels, fname):
		lnclrs = np.array(['#000000', '#ff0000', '#0000ff'])
		lss = ['-', '--', ':']
		fig, (ax1, ax2, ax3) = plt.subplots(figsize=(13, 4), ncols=3)
		pos1 = ax1.imshow(np.flipud(a), cmap='viridis', interpolation='nearest', extent=[0., 1., 0., 1.])
		ax1.set_title('a(x,y)')
		ax1.set_aspect('equal')
		fig.colorbar(pos1, ax=ax1, orientation="horizontal", pad=0.2)
		N, bins, patches = ax2.hist(np.log10(a).flatten(), bins=10)
		bin_centers = 0.5*(bins[:-1] + bins[1:])
		cols = bin_centers - min(bin_centers)
		cols /= max(cols)
		for col, patch in zip(cols, patches):
			color = plt.cm.viridis(col)
			patch.set_facecolor(color)
		ax2.set_title(r'Aperture log$_{10}$-distribution')
		alim = 0.
		for i, K in enumerate(Ks):
			ax3.add_patch(Ellipse(xy=(0, 0), width=K[0], height=K[1], angle=K[2], ls=lss[i], fill=False, color=lnclrs[i], label=labels[i]))
			alim = abs(max(K[0]*np.sin(K[2]), K[1]*np.sin(K[2]), K[0]*np.cos(K[2]), K[1]*np.cos(K[2]), (K[0] + K[1])/2., alim, key=abs))
		ax3.set_title('Princ. Components')
		ax3.set_aspect('equal')
		ax3.set_xlim(-alim/1.85, alim/1.85)
		ax3.set_ylim(-alim/1.85, alim/1.85)
		ax3.legend(loc='best')
		filedir = os.path.split(fname)[0]
		if not os.path.isdir(filedir):
			os.makedirs(filedir, exist_ok=True)
		fig.savefig(fname='{}.pdf'.format(fname))
		fig.savefig(fname='{}.png'.format(fname))
		plt.close()

	def plot_error(self):
		fname = os.path.join(self.workdir, 'plots', '{}_box_pres'.format(self.dist_type))
		np.random.shuffle(clrs)
		filedir = os.path.split(fname)[0]
		if not os.path.isdir(filedir):
			os.makedirs(filedir, exist_ok=True)
		fig = plt.figure(figsize=(15, 5))
		error = np.zeros((self.output_dim, self.M_test, self.M_states)).astype(float)
		ticks = np.empty(self.M_states).astype(str)
		for j in range(self.M_states):
			for i in range(self.output_dim):
				real = np.array([read_from_file(os.path.join(self.workdir, 'result_real', '{}_real_{}_{}'.format(self.data_names[i], j, k))) for k in range(self.M_test)])
				fake = np.array([read_from_file(os.path.join(self.workdir, 'result_fake', '{}_fake_{}_{}'.format(self.data_names[i], j, k))) for k in range(self.M_test)])
				error[i , :, j] = np.array([get_relative_error(real[k], fake[k], rmse=False) for k in range(self.M_test)])
			ticks[j] = human_format(float(self.state_files[j].split('_')[1].split('.')[0]))
		boxes = [None]*self.output_dim
		for i in range(self.output_dim):
			boxes[i] = plt.boxplot(error[i, :, 1:], positions=np.array(range(self.M_states-1))*self.output_dim+.8*(i-self.output_dim/2.+.5), showfliers=False, patch_artist=True, boxprops=dict(facecolor=clrs[i], color='k'), medianprops=dict(color='k'))
		plt.xticks(range(0, (self.M_states - 1)*self.output_dim, self.output_dim), ticks[1:])
		plt.suptitle('CGAM-ROM Outputs Error')
		ax = plt.gca()
		ax.yaxis.set_major_formatter(mtick.PercentFormatter())
		ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
		ax.set(axisbelow=True)
		plt.ylabel('Relative Error (%)')
		plt.xlabel('Steps')
		plt.legend([box['boxes'][0] for box in boxes], [self.titles[i] for i in range(self.output_dim)], loc='best', ncol=-(-(self.output_dim)//2))
		plt.savefig(fname='{}.png'.format(fname))
		plt.savefig(fname='{}.pdf'.format(fname))
		plt.close()
		fname = os.path.join(self.workdir, 'plots', '{}_box_pca'.format(self.dist_type))
		np.random.shuffle(clrs)
		filedir = os.path.split(fname)[0]
		if not os.path.isdir(filedir):
			os.makedirs(filedir, exist_ok=True)
		fig = plt.figure(figsize=(15, 5))
		error = np.zeros((3, self.M_test, self.M_states)).astype(float)
		ticks = np.empty(self.M_states).astype(str)
		for j in range(self.M_states):
			real = np.array([read_from_file(os.path.join(self.workdir, 'result_real', 'K_real_{}_{}'.format(j, k))) for k in range(self.M_test)])
			fake = np.array([read_from_file(os.path.join(self.workdir, 'result_fake', 'K_fake_{}_{}'.format(j, k))) for k in range(self.M_test)])
			error[0, :, j] = np.array([get_relative_error(real[k, 0], fake[k, 0]) for k in range(self.M_test)])
			error[1, :, j] = np.array([get_relative_error(real[k, 1], fake[k, 1]) for k in range(self.M_test)])
			error[2, :, j] = np.array([get_relative_error(real[k, 2], fake[k, 2], base=1e2) for k in range(self.M_test)])
			ticks[j] = human_format(float(self.state_files[j].split('_')[1].split('.')[0]))
		boxes = [None]*3
		ax1 = plt.gca()
		ax1.yaxis.set_major_formatter(mtick.PercentFormatter())
		ax1.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
		ax1.set(axisbelow=True)
		ax2 = ax1.twinx()
		for i in range(2):
			boxes[i] = ax1.boxplot(error[i, :, 1:], positions=np.array(range(self.M_states-1))*3+.8*(i-1), showfliers=False, patch_artist=True, boxprops=dict(facecolor=clrs[i], color='k'), medianprops=dict(color='k'))
		boxes[2] = ax2.boxplot(error[2, :, 1:], positions=np.array(range(self.M_states-1))*3+.8, showfliers=False, patch_artist=True, boxprops=dict(facecolor=clrs[2], color='k'), medianprops=dict(color='k'))
		ax1.set_ylabel('Relative Error (%)')
		ax2.set_ylabel(r'Princ. Orient. Error ($^\circ$)')
		plt.xticks(range(0, (self.M_states - 1)*3, 3), ticks[1:])
		plt.xlabel('Steps')
		plt.suptitle('Princ. Components Error')
		plt.legend([box['boxes'][0] for box in boxes], [r'$k_\mathrm{max}$', r'$k_\mathrm{min}$', r'$\theta$'], loc='best', ncol=3)
		plt.savefig(fname='{}.png'.format(fname))
		plt.savefig(fname='{}.pdf'.format(fname))
		plt.close()

	def plot_rfd_data(self, num_of_plots=1):
		for k, dname in enumerate(self.data_names):
			for j in np.random.randint(0, self.M_test, num_of_plots):
				condition = read_from_file(os.path.join(self.workdir, 'result_condition', 'condition_{}_{}'.format(self.selected_state, j)))
				real = read_from_file(os.path.join(self.workdir, 'result_real', '{}_real_{}_{}'.format(dname, self.selected_state, j)))
				fake = read_from_file(os.path.join(self.workdir, 'result_fake', '{}_fake_{}_{}'.format(dname, self.selected_state, j)))
				diff = read_from_file(os.path.join(self.workdir, 'result_diff', '{}_diff_{}_{}'.format(dname, self.selected_state, j)))
				fname = os.path.join(self.workdir, 'plots', '{}_rfd_{}_{}_{}'.format(self.dist_type, dname, j, self.selected_state))
				self.four_subplots(condition, 'log(a)', real, 'FEM-FOM: {}'.format(self.titles[k]), fake, 'CGAN-ROM: {}'.format(self.titles[k]), diff, 'Diff: {}'.format(self.titles[k]), fname=fname)

	def plot_principal_components(self, num_of_plots=1):
		for i in np.random.randint(0, self.M_test, int(num_of_plots)):
			aperture = 10**read_from_file(os.path.join(self.workdir, 'result_condition', 'condition_{}_{}'.format(self.selected_state, i)))
			real = read_from_file(os.path.join(self.workdir, 'result_real', 'K_real_{}_{}'.format(self.selected_state, i)))
			fake = read_from_file(os.path.join(self.workdir, 'result_fake', 'K_fake_{}_{}'.format(self.selected_state, i)))
			Ks = [real, fake]
			labels = ['FEM-FOM', 'CGAN-ROM']
			if self.dist_type == 'layered':
				a = read_from_file(os.path.join(self.workdir, 'aperture_test_data', 'a_{}'.format(i)))
				params = read_from_file(os.path.join(self.workdir, 'aperture_test_data', 'params_{}'.format(i)))
				theta = params[0]
				high_size = params[1]
				exact = get_exact_layered_tensor(a, theta, high_size)
				exact = get_principal_components(exact)
				Ks.insert(0, exact)
				labels.insert(0, 'Exact')
			fname = os.path.join(self.workdir, 'plots', '{}_pca_{}_{}'.format(self.dist_type, i, self.selected_state))
			self.ellipse_plot(aperture, Ks, labels, fname=fname)

	def scatter_error(self):
		real = np.array([read_from_file(os.path.join(self.workdir, 'result_real', 'K_real_{}_{}'.format(self.selected_state, k))) for k in range(self.M_test)])
		fake = np.array([read_from_file(os.path.join(self.workdir, 'result_fake', 'K_fake_{}_{}'.format(self.selected_state, k))) for k in range(self.M_test)])
		ratio = np.log10(np.abs(real[:, 0]/real[:, 1]))
		theta = real[:, 2]
		error = np.zeros((3, self.M_test)).astype(float)
		error[0, :] = np.array([get_relative_error(real[k, 0], fake[k, 0]) for k in range(self.M_test)])
		error[1, :] = np.array([get_relative_error(real[k, 1], fake[k, 1]) for k in range(self.M_test)])
		error[2, :] = np.array([get_relative_error(real[k, 2], fake[k, 2], base=1e2) for k in range(self.M_test)])
		nofliers = []
		for i in range(3):
			dist = np.abs(error[i, :] - np.median(error[i, :]))
			mdev = np.median(dist)
			nofliers.append(dist/mdev if mdev else 0.)
		fig, (ax1, ax2, ax3) = plt.subplots(figsize=(11, 4), ncols=3)
		pos1 = ax1.scatter(theta[nofliers[0] < 1.5], ratio[nofliers[0] < 1.5], c=error[0, nofliers[0] < 1.5], cmap='viridis')
		ax1.set_title('Max. component error')
		ax1.set_xlabel(r'$\theta$')
		ax1.set_ylabel(r'$O(k_\mathrm{max}/k_\mathrm{min})$')
		fig.colorbar(pos1, ax=ax1, orientation="horizontal", pad=0.2)
		pos2 = ax2.scatter(theta[nofliers[1] < 1.5], ratio[nofliers[1] < 1.5], c=error[1, nofliers[1] < 1.5], cmap='viridis')
		ax2.set_title('Min. component error')
		ax2.set_xlabel(r'$\theta$')
		ax2.set_ylabel(r'$O(k_\mathrm{max}/k_\mathrm{min})$')
		fig.colorbar(pos2, ax=ax2, orientation="horizontal", pad=0.2)
		pos3 = ax3.scatter(theta[nofliers[2] < 1.5], ratio[nofliers[2] < 1.5], c=error[2, nofliers[2] < 1.5], cmap='viridis')
		ax3.set_title('Princ. orientation error')
		ax3.set_xlabel(r'$\theta$')
		ax3.set_ylabel(r'$O(k_\mathrm{max}/k_\mathrm{min})$')
		fig.colorbar(pos3, ax=ax3, orientation="horizontal", pad=0.2)
		fig.tight_layout()
		plt.subplots_adjust(wspace=0.4)
		fname = os.path.join(self.workdir, 'plots', '{}_scatter'.format(self.dist_type))
		filedir = os.path.split(fname)[0]
		if not os.path.isdir(filedir):
			os.makedirs(filedir, exist_ok=True)
		plt.savefig(fname='{}.png'.format(fname))
		plt.savefig(fname='{}.pdf'.format(fname))
		plt.close()