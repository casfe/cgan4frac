import gstools as gs
import numpy as np
import os
from scipy.interpolate import griddata


class Distribution(object):
	def __init__(self, argsdict):
		super(Distribution, self).__init__()
		for key in argsdict.keys():
			setattr(self, key, argsdict[key])
		if not os.path.isdir(self.workdir):
			os.makedirs(self.workdir, exist_ok=True)

	def __call__(self, seed, rotate=True, angle=0.):
		if self.dist_type == 'layered':
			return self.layered_distribution(seed, rotate, angle)
		elif self.dist_type == 'zinn_harvey':
			return self.zinn_harvey_distribution(seed)
		elif self.dist_type == 'self_affine':
			return self.self_affine_distribution(seed, rotate, angle)
		else:
			raise ValueError('Invalid distribution type')

	def layered_distribution(self, seed, rotate=True, angle=0.):
		np.random.seed(seed)
		r = 4
		L = r*self.target_shape
		theta = np.random.sample()*np.pi if rotate else angle
		aperture = np.zeros((L + 1, L + 1))
		for i,y in enumerate(np.linspace(0., r, L + 1)):
			aperture[i, :] += np.cos(self.target_shape*np.pi*y/8)
		aperture = 1 - (aperture > -.5).astype(int)
		high_size = aperture[int((r/2 - .5)*self.target_shape):int((r/2 + .5)*self.target_shape), 0].sum()/self.target_shape
		X_0, Y_0 = np.meshgrid(np.linspace(0., r, L + 1), np.linspace(0., r, L + 1))
		X, Y = np.meshgrid(np.linspace(0., r, L), np.linspace(0., r, L))
		points_0 = np.array((X_0.flatten(), Y_0.flatten())).T - r/2
		Q = np.array([[np.cos(-theta), np.sin(-theta)],
					  [-np.sin(-theta), np.cos(-theta)]])
		points_0 = points_0@Q.T + r/2
		aperture = griddata(points_0, aperture.flatten(), (X, Y), method='linear')[int((r/2 - .5)*self.target_shape):int((r/2 + .5)*self.target_shape), int((r/2 - .5)*self.target_shape):int((r/2 + .5)*self.target_shape)]
		aperture = (aperture > 0).astype(int)
		aperture = (6*aperture + 1).astype(float)*1e-6
		return aperture, theta, high_size

	def zinn_harvey_distribution(self, seed):
		xx = np.linspace(0., 1., self.target_shape)
		yy = np.linspace(0., 1., self.target_shape)
		model = gs.Gaussian(dim=2, var=1., len_scale=.4)
		srf = gs.SRF(model, mean=1., seed=seed)
		srf.structured([xx, yy])
		srf.transform('zinnharvey', conn='high')
		aperture = srf.field - srf.field.min()
		aperture += 1e-4
		aperture *= 1e-6
		return aperture, None, None

	def self_affine_wall(self, theta):
		r = 4
		L = r*self.target_shape
		H = .8
		height = np.zeros((L + 1, L + 1))
		levels = int(np.log2(L) + 1)
		for level in range(levels):
			step = L//2**level
			for y in range(0, L + 1, step):
				jumpover = 1 - (y//step)%2 if level > 0 else 0
				for x in range(step*jumpover, L + 1, step*(1 + jumpover)):
					pointer = 1 - (x//step)%2 + 2*jumpover if level > 0 else 3
					yref, xref = step*(1 - pointer//2), step*(1 - pointer%2)
					corner1 = height[y - yref, x - xref]
					corner2 = height[y + yref, x + xref]
					average = (corner1 + corner2)/2.
					variation = step*(np.random.normal(scale=np.sqrt(1./2**(level*H))))
					height[y, x] = average + variation if level > 0 else 0
		X_0, Y_0 = np.meshgrid(np.linspace(0., 1., L + 1), np.linspace(0., 1., L + 1))
		X, Y = np.meshgrid(np.linspace(0., 1., L), np.linspace(0., 1., L))
		points_0 = np.array((X_0.flatten(), Y_0.flatten())).T - .5
		Q = np.array([[np.cos(theta), np.sin(theta)],
					  [-np.sin(theta), np.cos(theta)]])
		points_0 = points_0@Q.T + .5
		height = griddata(points_0, height.flatten(), (X, Y), method='linear')[int((r/2 - .5)*self.target_shape):int((r/2 + .5)*self.target_shape), int((r/2 - .5)*self.target_shape):int((r/2 + .5)*self.target_shape)]
		return (height - height.mean())/height.std()

	def self_affine_distribution(self, seed, rotate=True, angle=0.):
		np.random.seed(seed)
		theta = np.random.sample()*np.pi if rotate else angle
		wall_1 = self.self_affine_wall(theta)
		wall_2 = self.self_affine_wall(theta)
		aperture = wall_1 - wall_2
		aperture += 2*aperture.std()
		aperture[np.where(aperture < 1e-4)] = 1e-4
		aperture *= 1e-6
		return aperture, None, None