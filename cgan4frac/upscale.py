from .cgan import CGANModel, rotate_tensor, get_upscale_tensor, get_exact_layered_tensor, get_principal_components, fix_array_shape
from .network import Discriminator
from .io import write_to_file
import numpy as np
import os
import torch
from torch.utils.data import DataLoader, TensorDataset
from tqdm.auto import tqdm
torch.manual_seed(0)


class UpscaleModel(CGANModel):
	def __init__(self, argsdict):
		super(UpscaleModel, self).__init__(argsdict)
		self.avg_criterion = torch.nn.L1Loss()
		self.disc = Discriminator(self.input_dim + int(self.output_dim*3)).to(self.device)
		self.disc_opt = torch.optim.Adam(self.disc.parameters(), lr=self.lr, betas=(self.beta_1, self.beta_2))

	def __call__(self, aperture):
		inputs = np.stack([np.array([np.log10(aperture)])], axis=0)
		inputs = torch.from_numpy(self.norm_inputs(inputs))
		inputs = inputs.view(inputs.shape[0], self.input_dim, inputs.shape[2], inputs.shape[3])
		inputs = inputs.to(self.device)
		outputs = self.gen(inputs.float())
		outputs = self.inv_norm_outputs(outputs.cpu().detach().numpy())
		return self.get_outputs_variables(outputs)

	def get_outputs_variables(self, outputs):
		p1 = outputs[:, 0, :, :]
		p2 = outputs[:, 1, :, :]
		return fix_array_shape(p1, p2)

	def extend_output(self, output):
		grad_output = torch.cat(torch.gradient(output, spacing=1./self.target_shape, dim=(2, 3)), axis=1)
		return torch.cat((output, grad_output), axis=1)

	def get_gen_loss(self, real, condition):
		ext_real = self.extend_output(real)
		fake = self.gen(condition)
		ext_fake = self.extend_output(fake)
		pred = self.disc(ext_fake, condition)
		real_grad = ext_real[:, 2:, :, :]
		real_mean = real_grad.mean((2, 3))
		fake_grad = ext_fake[:, 2:, :, :]
		fake_mean = fake_grad.mean((2, 3))
		return -pred.mean() + self.lambda_recon*(self.recon_criterion(fake, real) + self.recon_criterion(fake_grad, real_grad) + .5*self.avg_criterion(fake_mean, real_mean))

	def train(self, save_model=False):
		torch_dataset = TensorDataset(self.inputs.float(), self.outputs.float())
		dataloader = DataLoader(dataset=torch_dataset, batch_size=self.train_batch_size, shuffle=True)
		mean_generator_loss = 0
		mean_discriminator_loss = 0
		cur_step = int(self.state_files[-1].split('_')[1].split('.')[0]) if self.state_files else 0
		gen_losses = dict()
		disc_losses = dict()
		for epoch in range(self.n_epochs):
			for condition, real in tqdm(dataloader):
				condition = condition.to(self.device)
				real = real.to(self.device)
				self.disc_opt.zero_grad()
				with torch.no_grad():
					fake = self.gen(condition)
				ext_real = self.extend_output(real)
				ext_fake = self.extend_output(fake)
				disc_fake_hat = self.disc(ext_fake.detach(), condition)
				disc_real_hat = self.disc(ext_real, condition)
				epsilon = torch.rand(len(real), int(self.output_dim*3), 1, 1, device=self.device, requires_grad=True)
				gp = self.get_gradient_penalty(ext_real, ext_fake.detach(), epsilon, condition)
				disc_loss = self.get_disc_loss(disc_fake_hat, disc_real_hat, gp)
				disc_loss.backward(retain_graph=True)
				self.disc_opt.step()
				self.gen_opt.zero_grad()
				gen_loss = self.get_gen_loss(real, condition)
				gen_loss.backward()
				self.gen_opt.step()
				self.gen_scheduler.step()
				mean_discriminator_loss += disc_loss.item()/self.display_step
				mean_generator_loss += gen_loss.item()/self.display_step
				gen_losses[cur_step] = gen_loss.item()
				disc_losses[cur_step] = disc_loss.item()
				if cur_step%self.display_step == 0:
					print('Iter-{0} lr: {1}'.format(cur_step, self.gen_opt.param_groups[0]['lr']))
					if cur_step > 0:
						print(f'Epoch {epoch}: Step {cur_step}: Generator (U-Net) loss: {mean_generator_loss}, Discriminator loss: {mean_discriminator_loss}')
					else:
						print('Pretrained initial state')
					mean_generator_loss = 0
					mean_discriminator_loss = 0
				if cur_step%self.save_step == 0 and save_model:
					torch.save({'gen': self.gen.state_dict(), 'gen_opt': self.gen_opt.state_dict(), 'disc': self.disc.state_dict(), 'disc_opt': self.disc_opt.state_dict() }, os.path.join(self.workdir, 'state', 'pix2pix_{}.pth'.format(cur_step)))
				cur_step += 1
		if save_model:
			torch.save({'gen': self.gen.state_dict(), 'gen_opt': self.gen_opt.state_dict(), 'disc': self.disc.state_dict(), 'disc_opt': self.disc_opt.state_dict() }, os.path.join(self.workdir, 'state', 'pix2pix_{}.pth'.format(cur_step)))
			write_to_file(os.path.join(self.workdir, 'gen_losses'), gen_losses)
			write_to_file(os.path.join(self.workdir, 'disc_losses'), disc_losses)

	def test(self):
		torch_dataset = TensorDataset(self.inputs.float(), self.outputs.float())
		dataloader = DataLoader(dataset=torch_dataset, batch_size=self.test_batch_size, shuffle=False)
		for gen_num, gen_name in enumerate(self.state_files):
			self.load_state(gen_num)
			cur_step = 0
			for condition, real in tqdm(dataloader):
				condition = condition.to(self.device)
				real = real.to(self.device)
				with torch.no_grad():
					fake = self.gen(condition)
				condition = self.inv_norm_inputs(condition.cpu().detach().numpy())
				fake = self.inv_norm_outputs(fake.cpu().detach().numpy())
				real = self.inv_norm_outputs(real.cpu().detach().numpy())
				for i in range(len(fake)):
					for j in range(self.output_dim):
						name = self.data_names[j]
						diff = np.abs(fake[i, j, :, :] - real[i, j, :, :])
						write_to_file(os.path.join(self.workdir, 'result_fake', '{}_fake_{}_{}'.format(name, gen_num, cur_step)), fake[i, j, :, :])
						write_to_file(os.path.join(self.workdir, 'result_real', '{}_real_{}_{}'.format(name, gen_num, cur_step)), real[i, j, :, :])
						write_to_file(os.path.join(self.workdir, 'result_diff', '{}_diff_{}_{}'.format(name, gen_num, cur_step)), diff)
					write_to_file(os.path.join(self.workdir, 'result_condition', 'condition_{}_{}'.format(gen_num, cur_step)), condition[i, 0, :, :])
					a = 10**condition[i, 0, :, :]
					p1, p2 = self.get_outputs_variables(np.array([fake[i, :, :, :]]))
					K_fake = get_upscale_tensor(a, p1, p2)
					K_fake = get_principal_components(K_fake)
					write_to_file(os.path.join(self.workdir, 'result_fake', 'K_fake_{}_{}'.format(gen_num, cur_step)), K_fake)
					p1, p2 = self.get_outputs_variables(np.array([real[i, :, :, :]]))
					K_real = get_upscale_tensor(a, p1, p2)
					K_real = get_principal_components(K_real)
					write_to_file(os.path.join(self.workdir, 'result_real', 'K_real_{}_{}'.format(gen_num, cur_step)), K_real)
					K_diff = np.abs(K_real - K_fake)
					write_to_file(os.path.join(self.workdir, 'result_diff', 'K_diff_{}_{}'.format(gen_num, cur_step)), K_diff)
					cur_step += 1