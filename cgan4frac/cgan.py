from .io import write_to_file
from .network import Discriminator, UNet, weights_init
import numpy as np
import os
import torch
from torch.utils.data import DataLoader, TensorDataset
from tqdm.auto import tqdm
torch.manual_seed(0)


def rotate_tensor(K, theta):
	Q = np.array([[np.cos(theta), np.sin(theta)],
				  [-np.sin(theta), np.cos(theta)]])
	return Q@K@Q.T
	
	
def get_upscale_tensor(a, p1, p2):
	mu = 1.e-3
	dyp1, dxp1 = np.gradient(p1, 1./len(p1))
	dyp2, dxp2 = np.gradient(p2, 1./len(p2))
	u1 = -a**2/(12*mu)*dxp1
	v1 = -a**2/(12*mu)*dyp1
	u2 = -a**2/(12*mu)*dxp2
	v2 = -a**2/(12*mu)*dyp2
	A = np.array([[dxp1.mean(), dyp1.mean(), 0],
				  [dxp2.mean(), dxp1.mean() + dyp2.mean(), dyp1.mean()],
				  [0, dxp2.mean(), dyp2.mean()]])
	b = -mu*np.array([u1.mean(), v1.mean() + u2.mean(), v2.mean()])
	K = np.linalg.solve(A, b)
	return np.asarray([K[0], K[1], K[1], K[2]]).reshape(2, 2)


def get_exact_layered_tensor(a, theta, high_size):
	k_high = a.max()**2/12.
	k_low = a.min()**2/12.
	k_x = high_size*k_high + (1 - high_size)*k_low
	k_y = 1./(high_size/k_high + (1 - high_size)/k_low)
	K = np.array([[k_x, 0],
				  [0, k_y]])
	return rotate_tensor(K, -theta)


def get_principal_components(K):
	theta = np.arctan2(2*K[0, 1], K[0, 0] - K[1, 1])/2
	K = rotate_tensor(K, theta)
	k_max = max(K[0, 0], K[1, 1])
	k_min = min(K[0, 0], K[1, 1])
	return np.array([k_max, k_min, np.degrees(theta)])


def get_relative_error(real, fake, base=False, rmse=False):
	if rmse:
		base = (real**2).sum() if not base else base
		return np.sqrt(((real - fake)**2).sum()/base)*100
	else:
		base = np.abs(real).sum() if not base else base
		return np.abs(real - fake).sum()/base*100


def fix_array_shape(*args):
	return tuple([(arg if len(arg) > 1 else arg[0]) for arg in args])


class CGANModel(object):
	def __init__(self, argsdict):
		super(CGANModel, self).__init__()
		for key in argsdict.keys():
			setattr(self, key, argsdict[key])
		if not os.path.isdir(self.workdir):
			os.makedirs(self.workdir, exist_ok=True)
		if torch.cuda.is_available():
			self.device = torch.device('cuda:0')
		else:
			self.device = torch.device('cpu')
		self.recon_criterion = torch.nn.L1Loss()
		if not os.path.isdir(os.path.join(self.workdir, 'state')):
			os.makedirs(os.path.join(self.workdir, 'state'), exist_ok=True)
			self.state_files = False
			self.save_network = True
		else:
			self.state_files = [f for f in os.listdir(os.path.join(self.workdir, 'state')) if os.path.isfile(os.path.join(self.workdir, 'state', f))]
			self.state_files.sort(key=lambda x: float(x.split('_')[1].split('.')[0]))
		self.gen = UNet(self.input_dim, self.output_dim).to(self.device)
		self.gen_opt = torch.optim.Adam(self.gen.parameters(), lr=self.lr, betas=(self.beta_1, self.beta_2))
		self.gen_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(self.gen_opt, 300*2500)
		self.disc = Discriminator(self.input_dim + self.output_dim).to(self.device)
		self.disc_opt = torch.optim.Adam(self.disc.parameters(), lr=self.lr, betas=(self.beta_1, self.beta_2))

	def init(self):
		if self.state_files:
			assert os.path.isfile(os.path.join(self.workdir, 'input_networks'))
			assert os.path.isfile(os.path.join(self.workdir, 'output_networks'))
			self.save_network = False
			self.input_limits = np.loadtxt(os.path.join(self.workdir, 'input_networks')).reshape(self.input_dim, 2)
			self.output_limits = np.loadtxt(os.path.join(self.workdir, 'output_networks')).reshape(self.output_dim, 2)
			self.selected_state = range(len(self.state_files))[self.selected_state]
			self.load_state(self.selected_state)
		else:
			self.gen = self.gen.apply(weights_init)
			self.disc = self.disc.apply(weights_init)
			self.save_network = True

	def load_state(self, state):
		loaded_state = torch.load(os.path.join(self.workdir, 'state', self.state_files[state]))
		self.gen.load_state_dict(loaded_state['gen'])
		self.gen_opt.load_state_dict(loaded_state['gen_opt'])
		self.disc.load_state_dict(loaded_state['disc'])
		self.disc_opt.load_state_dict(loaded_state['disc_opt'])

	def set_inputs(self, inputs):
		if self.save_network:
			self.input_limits = np.array([[inputs[:, i, :, :].min(), inputs[:, i, :, :].max()] for i in range(self.input_dim)]).reshape(self.input_dim, 2)
			write_to_file(os.path.join(self.workdir, 'input_networks'), self.input_limits, wtype='w', use_pickle=False)
		inputs = torch.from_numpy(self.norm_inputs(inputs))
		self.inputs = inputs.view(inputs.shape[0], self.input_dim, inputs.shape[2], inputs.shape[3])

	def set_outputs(self, outputs):
		if self.save_network:
			self.output_limits = np.array([[outputs[:, i, :, :].min(), outputs[:, i, :, :].max()] for i in range(self.output_dim)]).reshape(self.output_dim, 2)
			write_to_file(os.path.join(self.workdir, 'output_networks'), self.output_limits, wtype='w', use_pickle=False)
		outputs = torch.from_numpy(self.norm_outputs(outputs))
		self.outputs = outputs.view(outputs.shape[0], self.output_dim, outputs.shape[2], outputs.shape[3])

	def norm_inputs(self, inputs):
		for i in range(self.input_dim):
			inputs[:, i, :, :] = (inputs[:, i, :, :] - self.input_limits[i, 0])/(self.input_limits[i, 1] - self.input_limits[i, 0])
		return inputs

	def norm_outputs(self, outputs):
		for i in range(self.output_dim):
			outputs[:, i, :, :] = (outputs[:, i, :, :] - self.output_limits[i, 0])/(self.output_limits[i, 1] - self.output_limits[i, 0])
		return outputs

	def inv_norm_inputs(self, inputs):
		for i in range(self.input_dim):
			inputs[:, i, :, :] =  (self.input_limits[i, 1] - self.input_limits[i, 0])*inputs[:, i, :, :] + self.input_limits[i, 0]
		return inputs

	def inv_norm_outputs(self, outputs):
		for i in range(self.output_dim):
			outputs[:, i, :, :] =  (self.output_limits[i, 1] - self.output_limits[i, 0])*outputs[:, i, :, :] + self.output_limits[i, 0]
		return outputs

	def get_gen_loss(self, real, condition):
		fake = self.gen(condition)
		pred = self.disc(fake, condition)
		return -torch.mean(pred) + self.lambda_recon*self.recon_criterion(fake, real)

	def get_gradient_penalty(self, real, fake, epsilon, condition):
		mixed_images = real*epsilon + fake*(1 - epsilon)
		mixed_scores = self.disc(mixed_images, condition)
		gradient = torch.autograd.grad(inputs=mixed_images, outputs=mixed_scores, grad_outputs=torch.ones_like(mixed_scores), create_graph=True, retain_graph=True)[0]
		gradient = gradient.view(len(gradient), -1)
		gradient_norm = gradient.norm(2, dim=1)
		penalty = torch.mean(torch.pow(gradient_norm - torch.ones_like(gradient_norm), 2.0))
		return penalty

	def get_disc_loss(self, disc_fake_pred, disc_real_pred, gp):
		disc_loss = torch.mean(disc_fake_pred) - torch.mean(disc_real_pred) + self.c_lambda*gp
		return disc_loss

	def get_outputs_variables(self, outputs):
		p1 = outputs[:, 0, :, :]
		p2 = outputs[:, 1, :, :]
		return fix_array_shape(p1, p2)

	def train(self, save_model=False):
		torch_dataset = TensorDataset(self.inputs.float(), self.outputs.float())
		dataloader = DataLoader(dataset=torch_dataset, batch_size=self.train_batch_size, shuffle=True)
		mean_generator_loss = 0
		mean_discriminator_loss = 0
		cur_step = int(self.state_files[-1].split('_')[1].split('.')[0]) if self.state_files else 0
		gen_losses = dict()
		disc_losses = dict()
		for epoch in range(self.n_epochs):
			for condition, real in tqdm(dataloader):
				condition = condition.to(self.device)
				real = real.to(self.device)
				self.disc_opt.zero_grad()
				with torch.no_grad():
					fake = self.gen(condition)
				disc_fake_hat = self.disc(fake.detach(), condition)
				disc_real_hat = self.disc(real, condition)
				epsilon = torch.rand(len(real), self.output_dim, 1, 1, device=self.device, requires_grad=True)
				gp = self.get_gradient_penalty(real, fake.detach(), epsilon, condition)
				disc_loss = self.get_disc_loss(disc_fake_hat, disc_real_hat, gp)
				disc_loss.backward(retain_graph=True)
				self.disc_opt.step()
				self.gen_opt.zero_grad()
				gen_loss = self.get_gen_loss(real, condition)
				gen_loss.backward()
				self.gen_opt.step()
				self.gen_scheduler.step()
				mean_discriminator_loss += disc_loss.item()/self.display_step
				mean_generator_loss += gen_loss.item()/self.display_step
				gen_losses[cur_step] = gen_loss.item()
				disc_losses[cur_step] = disc_loss.item()
				if cur_step%self.display_step == 0:
					print('Iter-{0} lr: {1}'.format(cur_step, self.gen_opt.param_groups[0]['lr']))
					if cur_step > 0:
						print(f'Epoch {epoch}: Step {cur_step}: Generator (U-Net) loss: {mean_generator_loss}, Discriminator loss: {mean_discriminator_loss}')
					else:
						print('Pretrained initial state')
					mean_generator_loss = 0
					mean_discriminator_loss = 0
				if cur_step%self.save_step == 0 and save_model:
					torch.save({'gen': self.gen.state_dict(), 'gen_opt': self.gen_opt.state_dict(), 'disc': self.disc.state_dict(), 'disc_opt': self.disc_opt.state_dict() }, os.path.join(self.workdir, 'state', 'pix2pix_{}.pth'.format(cur_step)))
				cur_step += 1
		if save_model:
			torch.save({'gen': self.gen.state_dict(), 'gen_opt': self.gen_opt.state_dict(), 'disc': self.disc.state_dict(), 'disc_opt': self.disc_opt.state_dict() }, os.path.join(self.workdir, 'state', 'pix2pix_{}.pth'.format(cur_step)))
			write_to_file(os.path.join(self.workdir, 'gen_losses'), gen_losses)
			write_to_file(os.path.join(self.workdir, 'disc_losses'), disc_losses)

	def test(self, upscale=True):
		torch_dataset = TensorDataset(self.inputs.float(), self.outputs.float())
		dataloader = DataLoader(dataset=torch_dataset, batch_size=self.test_batch_size, shuffle=False)
		for gen_num, gen_name in enumerate(self.state_files):
			self.load_state(gen_num)
			cur_step = 0
			for condition, real in tqdm(dataloader):
				condition = condition.to(self.device)
				real = real.to(self.device)
				with torch.no_grad():
					fake = self.gen(condition)
				condition = self.inv_norm_inputs(condition.cpu().detach().numpy())
				fake = self.inv_norm_outputs(fake.cpu().detach().numpy())
				real = self.inv_norm_outputs(real.cpu().detach().numpy())
				for i in range(len(fake)):
					for j in range(self.output_dim):
						name = self.data_names[j]
						diff = np.abs(fake[i, j, :, :] - real[i, j, :, :])
						write_to_file(os.path.join(self.workdir, 'result_fake', '{}_fake_{}_{}'.format(name, gen_num, cur_step)), fake[i, j, :, :])
						write_to_file(os.path.join(self.workdir, 'result_real', '{}_real_{}_{}'.format(name, gen_num, cur_step)), real[i, j, :, :])
						write_to_file(os.path.join(self.workdir, 'result_diff', '{}_diff_{}_{}'.format(name, gen_num, cur_step)), diff)
					write_to_file(os.path.join(self.workdir, 'result_condition', 'condition_{}_{}'.format(gen_num, cur_step)), condition[i, 0, :, :])
					if upscale:
						a = 10**condition[i, 0, :, :]
						p1, p2 = self.get_outputs_variables(np.array([fake[i, :, :, :]]))
						K_fake = get_upscale_tensor(a, p1, p2)
						K_fake = get_principal_components(K_fake)
						write_to_file(os.path.join(self.workdir, 'result_fake', 'K_fake_{}_{}'.format(gen_num, cur_step)), K_fake)
						p1, p2 = self.get_outputs_variables(np.array([real[i, :, :, :]]))
						K_real = get_upscale_tensor(a, p1, p2)
						K_real = get_principal_components(K_real)
						write_to_file(os.path.join(self.workdir, 'result_real', 'K_real_{}_{}'.format(gen_num, cur_step)), K_real)
						K_diff = np.abs(K_real - K_fake)
						write_to_file(os.path.join(self.workdir, 'result_diff', 'K_diff_{}_{}'.format(gen_num, cur_step)), K_diff)
					cur_step += 1