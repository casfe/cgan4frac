import os

'''
	Model 1:
		- Layered aperture profiles as input
		- Train data set with 5K realizations
		- Test data set contains 500 realizations
		- States are saved every 25K training steps

	Model 2:
		- Zinn & Harvey transformation for aperture as input
		- Train data set with 10K realizations
		- Test data set with 1K realizations
		- States are saved every 50K steps

	Model 3:
		- Self-affine fractal distributions for aperture as input
		- Train data set with 10K realizations
		- Test data set with 1K realizations
		- States are saved every 10K steps
'''

model_params = {}
model_params['layered'] = {'dist_type': 'layered', 'workdir': os.path.join('models', 'layered'), 'input_dim': 1, 'output_dim': 2, 'target_shape': 128, 'M_train': 5000, 'train_batch_size': 5, 'M_test': 500, 'test_batch_size': 500, 'n_epochs': 500, 'display_step': 5000, 'save_step': 25000, 'lambda_recon': 500, 'lr': 0.0001, 'beta_1': 0.5, 'beta_2': 0.999, 'c_lambda': 10, 'selected_state': -1, 'data_names': ['p1', 'p2'], 'titles': ['$p_1$', '$p_2$']}
model_params['zinn_harvey'] = {'dist_type': 'zinn_harvey', 'workdir': os.path.join('models', 'zinn_harvey'), 'input_dim': 1, 'output_dim': 2, 'target_shape': 128, 'M_train': 10000, 'train_batch_size': 5, 'M_test': 1000, 'test_batch_size': 500, 'n_epochs': 250, 'display_step': 10000, 'save_step': 50000, 'lambda_recon': 500, 'lr': 0.0001, 'beta_1': 0.5, 'beta_2': 0.999, 'c_lambda': 10, 'selected_state': -1, 'data_names': ['p1', 'p2'], 'titles': ['$p_1$', '$p_2$']}
model_params['self_affine'] = {'dist_type': 'self_affine', 'workdir': os.path.join('models', 'self_affine'), 'input_dim': 1, 'output_dim': 2, 'target_shape': 128, 'M_train': 10000, 'train_batch_size': 5, 'M_test': 1000, 'test_batch_size': 500, 'n_epochs': 250, 'display_step': 10000, 'save_step': 50000, 'lambda_recon': 500, 'lr': 0.0001, 'beta_1': 0.5, 'beta_2': 0.999, 'c_lambda': 10, 'selected_state': -1, 'data_names': ['p1', 'p2'], 'titles': ['$p_1$', '$p_2$']}