import argparse
import gstools as gs
import os

from parameters import *


parser = argparse.ArgumentParser()
parser.add_argument('--action', required=True, type=str)
parser.add_argument('--dist-type', type=str)
parser.add_argument('--set-name', type=str)
parser.add_argument('--batch-start', type=int)
parser.add_argument('--batch-end', type=int)
parser.add_argument('--seed', type=int)
parser.add_argument('--num-of-plots', type=int)
args = parser.parse_args()
action = args.action
set_name = args.set_name if not args.set_name is None else None
batch_start = args.batch_start if not args.batch_start is None else 0
batch_end = args.batch_end if not args.batch_end is None else 1e99
seed = gs.random.MasterRNG(args.seed if not args.seed is None else None)
num_of_plots = args.num_of_plots if not args.num_of_plots is None else 1
model_params = model_params[args.dist_type] if not args.dist_type is None else model_params
if __name__ == '__main__':
	if action == 'generate data':
		print(model_params)
		assert not set_name is None
		from cgan4frac.distribution import *
		from cgan4frac.fem import *
		from cgan4frac.io import *
		from cgan4frac.timeit import *
		if set_name == 'train_data':
			batch_end = min(batch_end, model_params['M_train'])
		elif set_name == 'test_data':
			batch_end = min(batch_end, model_params['M_test'])
		else:
			raise ValueError('Invalid set name')
		dist = Distribution(model_params)
		fom = FEMModel(model_params)
		timeit = TimeIt()
		with timeit.timethis():
			for i in range(batch_start, batch_end):
				a, theta, high_size = dist(seed())
				write_to_file(os.path.join(model_params['workdir'], 'aperture_{}'.format(set_name), 'a_{}'.format(i)), a)
				write_to_file(os.path.join(model_params['workdir'], 'aperture_{}'.format(set_name), 'params_{}'.format(i)), (theta, high_size))
				p1, p2 = fom(a)
				write_to_file(os.path.join(model_params['workdir'], 'pressure_{}'.format(set_name), 'p1_{}'.format(i)), p1)
				write_to_file(os.path.join(model_params['workdir'], 'pressure_{}'.format(set_name), 'p2_{}'.format(i)), p2)
		print('Time for generating {} set: {:.2f} s'.format(set_name, timeit.get_run_time()))
	elif action == 'train rom':
		print(model_params)
		from cgan4frac.io import *
		from cgan4frac.upscale import *
		from cgan4frac.timeit import *
		rom = UpscaleModel(model_params)
		rom.init()
		a = np.array([read_from_file(os.path.join(model_params['workdir'], 'aperture_train_data', 'a_{}'.format(i))) for i in range(model_params['M_train'])])
		rom.set_inputs(np.stack([np.log10(a)], axis=1))
		del a
		p1 = np.array([read_from_file(os.path.join(model_params['workdir'], 'pressure_train_data', 'p1_{}'.format(i))) for i in range(model_params['M_train'])])
		p2 = np.array([read_from_file(os.path.join(model_params['workdir'], 'pressure_train_data', 'p2_{}'.format(i))) for i in range(model_params['M_train'])])
		rom.set_outputs(np.stack([p1, p2], axis=1))
		del p1, p2
		timeit = TimeIt()
		with timeit.timethis():
			rom.train(save_model=True)
		print('Time for training: {:.2f} s'.format(timeit.get_run_time()))
	elif action == 'test rom':
		print(model_params)
		from cgan4frac.io import *
		from cgan4frac.upscale import *
		from cgan4frac.timeit import *
		rom = UpscaleModel(model_params)
		timeit = TimeIt()
		rom.init()
		a = np.array([read_from_file(os.path.join(model_params['workdir'], 'aperture_test_data', 'a_{}'.format(i))) for i in range(model_params['M_test'])])
		rom.set_inputs(np.stack([np.log10(a)], axis=1))
		del a
		p1 = np.array([read_from_file(os.path.join(model_params['workdir'], 'pressure_test_data', 'p1_{}'.format(i))) for i in range(model_params['M_test'])])
		p2 = np.array([read_from_file(os.path.join(model_params['workdir'], 'pressure_test_data', 'p2_{}'.format(i))) for i in range(model_params['M_test'])])
		rom.set_outputs(np.stack([p1, p2], axis=1))
		del p1, p2
		with timeit.timethis():
			rom.test()
		print('Time for testing: {:.2f} s'.format(timeit.get_run_time()))
	elif action == 'plot results':
		print(model_params)
		from cgan4frac.visualization import *
		plotter = DataPlotter(model_params)
		plotter.plot_error()
		plotter.plot_rfd_data(num_of_plots)
		plotter.plot_principal_components(num_of_plots)
		plotter.scatter_error()
	elif action == 'time-it':
		from cgan4frac.fem import *
		from cgan4frac.io import *
		from cgan4frac.upscale import *
		from cgan4frac.timeit import *
		timeit = TimeIt()
		batch_size = 50
		for run in range(20):
			for key in model_params.keys():
				test_batch = np.random.randint(0, model_params[key]['M_test'], batch_size)
				a_batch = np.array([read_from_file(os.path.join(model_params[key]['workdir'], 'aperture_test_data', 'a_{}'.format(i))) for i in test_batch])
				rom = UpscaleModel(model_params[key])
				rom.init()
				with timeit.timethis():
					for a in a_batch:
						p1, p2 = rom(a)
						K = get_upscale_tensor(a, p1, p2)
						K = get_principal_components(K)
				run_time = np.array([batch_size, timeit.get_run_time()])
				try:
					run_time = np.concatenate([np.loadtxt(os.path.join(model_params[key]['workdir'], 'runtime_CGAN_ROM.txt')).reshape((-1, 2)), run_time.reshape((1, 2))])
				except FileNotFoundError:
					run_time =  run_time.reshape((1, 2))
				np.savetxt(os.path.join(model_params[key]['workdir'], 'runtime_CGAN_ROM.txt'), run_time)
				del rom
				fom = FEMModel(model_params[key])
				with timeit.timethis():
					for a in a_batch:
						p1, p2 = fom(a)
						K = get_upscale_tensor(a, p1, p2)
						K = get_principal_components(K)
				run_time = np.array([batch_size, timeit.get_run_time()])
				try:
					run_time = np.concatenate([np.loadtxt(os.path.join(model_params[key]['workdir'], 'runtime_FEM_FOM.txt')).reshape((-1, 2)), run_time.reshape((1, 2))])
				except FileNotFoundError:
					run_time =  run_time.reshape((1, 2))
				np.savetxt(os.path.join(model_params[key]['workdir'], 'runtime_FEM_FOM.txt'), run_time)
				del fom
				print('Run {}, dist_type:{}'.format(run, model_params[key]['dist_type']))
	else:
		raise ValueError('Invalid action')