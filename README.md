# cgan4frac

These scripts are part of the paper <b>A framework for upscaling and modelling fluid flow for discrete fractures using conditional generative adversarial networks</b>.

Ferreira, C. A. S., Kadeethum, T., Bouklas, N., Nick, H. M., 2022. A framework for upscaling and modelling fluid flow for discrete fractures using conditional generative adversarial networks. Advances in Water Resources, p. 104264. doi: [10.1016/j.advwatres.2022.104264](https://doi.org/10.1016/j.advwatres.2022.104264)

## Dependencies

The framework depends on and has been tested with:
- [Python 3](https://www.python.org/) (3.6.10);
- [CUDA](https://developer.nvidia.com/)(11.3);
- [FEniCS](https://fenicsproject.org/) (2019.1.0);
- [GSTools](https://github.com/GeoStat-Framework/GSTools)(1.3.3);
- [Matplotlib](https://matplotlib.org/)(3.3.4);
- [NumPy](https://numpy.org/) (1.16.5);
- [PETsc](https://petsc.org/	)(3.10.5);
- [PyTorch](https://pytorch.org/)(1.10.0);
- [SciPy](https://scipy.org/)(1.5.4);
- [tqdm](https://github.com/tqdm/tqdm)(4.60.0).

## Usage

### Parameters
First, define the parameters for each model in the file "parameters.py":
```python
...
model_params['self_affine'] = {'dist_type': 'self_affine', 'workdir': os.path.join('models', 'self_affine'), 'input_dim': 1, 'output_dim': 2, 'target_shape': 128, 'M_train': 10000, 'train_batch_size': 5, 'M_test': 1000, 'test_batch_size': 500, 'n_epochs': 250, 'display_step': 10000, 'save_step': 50000, 'lambda_recon': 500, 'lr': 0.0001, 'beta_1': 0.5, 'beta_2': 0.999, 'c_lambda': 10, 'selected_state': -1, 'data_names': ['p1', 'p2'], 'titles': ['$p_1$', '$p_2$']} # Model 3
...
```
The set-up values are:
- dist_type: 'layered', 'zinn_harvey' or 'self_affine';
- workdir: directory where the data sets, model files and test results will be stored;
- input_dim: integer representing the dimension of the input (1 in this work);
- output_dim: integer representing the dimension of the output (2 in this work);
- target_shape: the resolution of the images used in the image-to-image translation;
- M_train: the size of the train data set;
- train_batch_size: the size of the batch used in each train step;
- M_test: the size of the test data set;
- test_batch_size: the size of the batch used in each test step;
- n_epochs: the number of epochs for training;
- display_step: when display_step is reached, the code will print the current loss state of the training;
- save_step: the model will save a new training state when save_step is reached;
- lambda_recon, lr, beta_1, beta_2 and c_lambda: float numbers, penalties used in the model;
- selected_state: the training state used for visualization;
- data_names and titles: strings used for saving the results and in the plot titles.

### Data sets
Generate the data set by running
```bash
$ python3 main.py --dist-type ${dist_type} --action 'generate data' --set-name ${set_name} --batch-start ${batch_start}  --batch-end ${batch_end} --seed ${seed}
```
The set-up values are:
- set_name: 'train_data' (or 'test_data');
- batch_start: integer number, should be between 0 and M_train (or M_test);
- batch_end: integer number, should be lesser than M_train (or M_test);
- seed: integer number to allow reproducibility.

### Training & Testing
For training or testing, simply run:
```bash
$ python3 main.py --dist-type ${dist_type} --action ${action}
```
If action is set "train rom", the model is trained considering parameters.py, whereas "test rom" is used for testing the model.

### Visualization
To visualize the results, simply run:
```bash
$ python3 main.py --dist-type ${dist_type} --action 'plot results' --num-of-plots ${num_of_plots}
```
Here, num_of_plots is used for the number of random plots to be generated, <i>i.e.</i> for visualizing the predictions of the model and the equivalent tensor.
